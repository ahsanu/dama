create table Customer(
    idCustomer varchar(20) not null,
    nama varchar(10),
    alamat varchar(10),
    kota varchar(10),
    statusNikah varchar(20),
    jumlahTanggungan int, 
    pekerjaan varchar(20),
    gaji int,
    primary key(idCustomer)
);