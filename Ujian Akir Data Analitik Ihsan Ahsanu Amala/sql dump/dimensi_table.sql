-- Table Dimensi Pinjaman Nasabah
create table nilaiLoan_dim(
	nilaiLoan int,
    primary key(nilaiLoan)
);

create table cabang_dim(
	cabang varchar(20),
    primary key(cabang)
);

create table tanggal_dim(
	tanggal date,
    primary key(tanggal)
);

create table jumlahTanggungan_dim(
	jumlahTanggungan int(20),
	primary key(jumlahTanggungan)
);

create table gaji_dim(
	gaji int(20),
    primary key (gaji)
);

create table pekerjaan_dim(
	pekerjaan varchar(20),
    primary key(pekerjaan)
);

create table statusNikah_dim(
	statusNikah varchar(20),
    primary key(statusNikah)
);

create table customer_dim(
	idCustomer varchar(20),
    primary key(idCustomer)
);



insert into statusNikah_dim select distinct statusNikah from pinjaman_nasabah.customer;
insert into pekerjaan_dim select distinct pekerjaan from pinjaman_nasabah.customer;
insert into gaji_dim select distinct gaji from pinjaman_nasabah.customer;
insert into jumlahtanggungan_dim select distinct jumlahTanggungan from pinjaman_nasabah.customer;
insert into tanggal_dim values('2022-11-16');
insert into cabang_dim select distinct cabang from pinjaman_nasabah.pinjaman;
insert into nilaiLoan_dim select distinct nilaiLoan from pinjaman_nasabah.pinjaman;