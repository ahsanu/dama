create table pinjaman(
    idPinjaman int not null,
    idCustomer varchar(20),
    tanggalPinjam date,
    cabang varchar(20),
    nilaiLoan int,
    primary key(idPinjaman),
    foreign key(idCustomer) references Customer(idCustomer)
);