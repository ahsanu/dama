create table Customer(
	idCustomer varchar(20) not null,
    nama varchar(10),
    alamat varchar(10),
    kota varchar(10),
    statusNikah varchar(20),
    jumlahTanggungan int, 
    pekerjaan varchar(20),
    gaji int,
    primary key(idCustomer)
);

create table pinjaman(
	idPinjaman int not null,
    idCustomer varchar(20),
    tanggalPinjam date,
    cabang varchar(20),
    nilaiLoan int,
    primary key(idPinjaman),
    foreign key(idCustomer) references Customer(idCustomer)
);

create table cicilan(
	idCicilan int not null,
    idPinjaman int,
    tanggalCicil date, 
    nilaiCicil int,
	primary key (idCicilan),
    foreign key(idPinjaman) references pinjaman(idPinjaman)
);

select * from cicilan;
	
-- Table Dimensi Pinjaman Nasabah
create table nilaiLoan_dim(
	nilaiLoan int,
    primary key(nilaiLoan)
);

create table cabang_dim(
	cabang varchar(20),
    primary key(cabang)
);

create table tanggal_dim(
	tanggal date,
    primary key(tanggal)
);

create table jumlahTanggungan_dim(
	jumlahTanggungan int(20),
	primary key(jumlahTanggungan)
);

create table gaji_dim(
	gaji int(20),
    primary key (gaji)
);

create table pekerjaan_dim(
	pekerjaan varchar(20),
    primary key(pekerjaan)
);

create table statusNikah_dim(
	statusNikah varchar(20),
    primary key(statusNikah)
);

create table customer_dim(
	idCustomer varchar(20),
    primary key(idCustomer)
);



insert into statusNikah_dim select distinct statusNikah from pinjaman_nasabah.customer;
insert into pekerjaan_dim select distinct pekerjaan from pinjaman_nasabah.customer;
insert into gaji_dim select distinct gaji from pinjaman_nasabah.customer;
insert into jumlahtanggungan_dim select distinct jumlahTanggungan from pinjaman_nasabah.customer;
insert into tanggal_dim values('2022-11-16');
insert into cabang_dim select distinct cabang from pinjaman_nasabah.pinjaman;
insert into nilaiLoan_dim select distinct nilaiLoan from pinjaman_nasabah.pinjaman;
-- -------------- 


drop table statusNikah;
drop table pekerjaan;
drop table gaji;
drop table jumlahTanggungan;
drop table tanggal;
drop table cabang;
drop table nilaiLoan;

drop table fact_data_warehouse;
create table fact_data_warehouse(
	cabang varchar(20),
    tanggal date,
    idCustomer varchar(20),
    statusNikah varchar(20),
    jumlahTanggungan varchar(20), 
    pekerjaan varchar(20),
    gaji varchar(20), 
    nilaiLoan int, 
    terbayar int, 
    status varchar(20),
    foreign key(nilaiLoan) references NilaLoan_dim,
    foreign key(cabang) references cabang_dim,
    foreign key(tanggal) references tanggal_dim,
    foreign key(jumlahTanggungan) references jumlahtanggungan_dim,
    foreign key(gaji) references gaji_dim,
    foreign key(pekerjaan) references pekerjaan_dim,
    foreign key(statusNikah) references statusnikah_dim,
    foreign key(customer) references customer_dim
);

select * from fact_data_warehouse;

select * from customer;
select * from pinjaman;
select * from cicilan;

-- select pinjaman.cabang,NOW(),customer.IDCustomer,
-- MAX(cicilan.TanggalCicil) as tanggal_terahir_cicil,NilaiLoan - cicil as sisa_pinjaman
-- from pinjaman,cicilan,customer,
-- (select IDPinjaman,sum(NilaiCicil) as cicil from cicilan group by IDPinjaman) total_cicil
-- where total_cicil.IDPinjaman = pinjaman.IDPinjaman and cicilan.IDPinjaman = pinjaman.IDPinjaman
-- and pinjaman.IDCustomer = customer.IDCustomer
-- group by pinjaman.IDPinjaman;

select 
case  when jumlahTanggungan >=0 and jumlahTanggungan<=2 Then 'Sedikit'
when jumlahTanggungan >=3 and jumlahTanggungan<=5 Then 'Sedang'
else 'Besar'
end as jumlahTanggungan
from pinjaman_nasabah.customer;

select * from cicilan;

select case when sum(nilaicicil) = pinjaman.nilaiLoan then 'LUNAS'
when sum(nilaicicil) < pinjaman.nilaiLoan and (now()-max(cicilan.tanggalcicil))<30 then 'BELUM'
when sum(nilaicicil) < pinjaman.nilaiLoan and (now()-max(cicilan.tanggalcicil))>30 then 'GAGAL'
end as status from pinjaman join cicilan on pinjaman.idPinjaman = cicilan.idPinjaman; 
select gaji,
case  when gaji <=5 Then 'Kecil'
when gaji >=5 and gaji<=15 Then 'Sedang'
else 'Besar'
end as gaji
from pinjaman_nasabah.customer;


select sum(nilaiCicil) as Terbayar from cicilan group by(idPinjaman);
select * from cicilan;

select * from fact_data_warehouse;

insert into fact_data_warehouse select cabang, 
('2022-11-16'),customer.idcustomer, statusNikah,
case when jumlahTanggungan >=0 and jumlahTanggungan<=2 Then 'Sedikit'
when jumlahTanggungan >=3 and jumlahTanggungan<=5 Then 'Sedang'
else 'Besar' end as jumlahTanggungan,pekerjaan,case when gaji <=5 Then 'Kecil'
when gaji >=5 and gaji<=15 Then 'Sedang'else 'Besar' end as gaji,nilaiLoan,
sum(nilaiCicil) as Terbayar  ,
case when sum(nilaicicil) = pinjaman.nilaiLoan then 'LUNAS'
when sum(nilaicicil) < pinjaman.nilaiLoan and (now()-max(cicilan.tanggalcicil))<30 then 'BELUM'
when sum(nilaicicil) < pinjaman.nilaiLoan and (now()-max(cicilan.tanggalcicil))>30 then 'GAGAL'
end as status from pinjaman_nasabah.pinjaman join pinjaman_nasabah.customer
on pinjaman_nasabah.pinjaman.idCustomer = pinjaman_nasabah.customer.idCustomer
join pinjaman_nasabah.cicilan on pinjaman_nasabah.cicilan.idPinjaman = pinjaman_nasabah.pinjaman.idPinjaman
group by(pinjaman_nasabah.cicilan.idPinjaman);

select * from fact_data_warehouse;

select cabang, customer.idcustomer, statusNikah,
case when jumlahTanggungan >=0 and jumlahTanggungan<=2 Then 'Sedikit'
when jumlahTanggungan >=3 and jumlahTanggungan<=5 Then 'Sedang'
else 'Besar' end as jumlahTanggungan,pekerjaan,case when gaji <=5 Then 'Kecil'
when gaji >=5 and gaji<=15 Then 'Sedang'else 'Besar' end as gaji,nilaiLoan,
sum(nilaiCicil) as Terbayar  ,
case when sum(nilaicicil) = pinjaman.nilaiLoan then 'LUNAS'
when sum(nilaicicil) < pinjaman.nilaiLoan and (now()-max(cicilan.tanggalcicil))<30 then 'BELUM'
when sum(nilaicicil) < pinjaman.nilaiLoan and (now()-max(cicilan.tanggalcicil))>30 then 'GAGAL'
end as status from pinjaman_nasabah.pinjaman join pinjaman_nasabah.customer
on pinjaman_nasabah.pinjaman.idCustomer = pinjaman_nasabah.customer.idCustomer
join pinjaman_nasabah.cicilan on pinjaman_nasabah.cicilan.idPinjaman = pinjaman_nasabah.pinjaman.idPinjaman
group by(pinjaman_nasabah.cicilan.idPinjaman);

select sum(nilaiCicil) as Terbayar from cicilan group by(idPinjaman) 
