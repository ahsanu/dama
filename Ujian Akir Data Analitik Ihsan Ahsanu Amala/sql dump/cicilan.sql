create table cicilan(
    idCicilan int not null,
    idPinjaman int,
    tanggalCicil date, 
    nilaiCicil int,
    primary key (idCicilan),
    foreign key(idPinjaman) references pinjaman(idPinjaman)
);