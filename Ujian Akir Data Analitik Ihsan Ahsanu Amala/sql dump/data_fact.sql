create table fact_data_warehouse(
	cabang varchar(20),
    tanggal date,
    idCustomer varchar(20),
    statusNikah varchar(20),
    jumlahTanggungan varchar(20), 
    pekerjaan varchar(20),
    gaji varchar(20), 
    nilaiLoan int, 
    terbayar int, 
    status varchar(20),
    foreign key(nilaiLoan) references NilaLoan_dim,
    foreign key(cabang) references cabang_dim,
    foreign key(tanggal) references tanggal_dim,
    foreign key(jumlahTanggungan) references jumlahtanggungan_dim,
    foreign key(gaji) references gaji_dim,
    foreign key(pekerjaan) references pekerjaan_dim,
    foreign key(statusNikah) references statusnikah_dim,
    foreign key(customer) references customer_dim
);


insert into fact_data_warehouse select cabang, 
('2022-11-16'),customer.idcustomer, statusNikah,
case when jumlahTanggungan >=0 and jumlahTanggungan<=2 Then 'Sedikit'
when jumlahTanggungan >=3 and jumlahTanggungan<=5 Then 'Sedang'
else 'Besar' end as jumlahTanggungan,pekerjaan,case when gaji <=5 Then 'Kecil'
when gaji >=5 and gaji<=15 Then 'Sedang'else 'Besar' end as gaji,nilaiLoan,
sum(nilaiCicil) as Terbayar  ,
case when sum(nilaicicil) = pinjaman.nilaiLoan then 'LUNAS'
when sum(nilaicicil) < pinjaman.nilaiLoan and (now()-max(cicilan.tanggalcicil))<30 then 'BELUM'
when sum(nilaicicil) < pinjaman.nilaiLoan and (now()-max(cicilan.tanggalcicil))>30 then 'GAGAL'
end as status from pinjaman_nasabah.pinjaman join pinjaman_nasabah.customer
on pinjaman_nasabah.pinjaman.idCustomer = pinjaman_nasabah.customer.idCustomer
join pinjaman_nasabah.cicilan on pinjaman_nasabah.cicilan.idPinjaman = pinjaman_nasabah.pinjaman.idPinjaman
group by(pinjaman_nasabah.cicilan.idPinjaman);
