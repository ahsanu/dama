import tkinter as tk
from tkinter import ttk
from tkinter.messagebox import showerror

# Libray Machine Learning
from sklearn.neighbors import KNeighborsClassifier
# import pickle
import numpy as np


# root window
root = tk.Tk()
root.title('Prediksi Pemain Bola/Petinju')
root.geometry('500x200')
root.resizable(False, False)


# def fahrenheit_to_celsius(f):
#     """ Convert fahrenheit to celsius
#     """
#     return (f - 32) * 5/9

def predictProfes(tinggi,berat):
    data = [[170, 77],
            [195, 95],
            [167, 75],
            [185, 85],
            [205, 92],
            [179, 80],
            [175, 93]]
    status = [['pbola'],
            ['petinju'],
            ['pbola'],
            ['petinju'],
            ['petinju'],
            ['pbola'],
            ['petinju']]
    X_train = data
    Y_train = status

    knn = KNeighborsClassifier(n_neighbors=1) 
    knn.fit(X_train, Y_train)
    to_predict = np.array([tinggi,berat]).reshape(-1,2)
    prediksi = knn.predict(to_predict)[0]
    return prediksi   

# frame
frame = ttk.Frame(root)


# field options
options = {'padx': 1, 'pady': 1}

# temperature label
tinggi_label = ttk.Label(frame, text='Tinggi')
berat_label = ttk.Label(frame, text='Berat')
tinggi_label.grid(column=0, row=0, sticky='W', **options)
berat_label.grid(column=0, row=1, sticky='W', **options)

# temperature entry
tinggi = tk.StringVar()
tinggi_entry = ttk.Entry(frame, textvariable=tinggi)
tinggi_entry.grid(column=1, row=0, **options)
tinggi_entry.focus()

berat = tk.StringVar()
berat_entry = ttk.Entry(frame, textvariable=berat)
berat_entry.grid(column=1, row=1, **options)
berat_entry.focus()

# convert button

def convert_button_clicked():
    """  Handle convert button click event 
    """
    try:
        t = float(tinggi.get())
        b = float(berat.get())
        c = predictProfes(t,b)
        result = c
        result_label.config(text=result)
    except ValueError as error:
        showerror(title='Error', message=error)


convert_button = ttk.Button(frame, text='Prediksi')
convert_button.grid(column=2, row=0, sticky='W', **options)
convert_button.configure(command=convert_button_clicked)

# result label
result_label = ttk.Label(frame)
result_label.grid(row=4, columnspan=1, **options)

# add padding to the frame and show it
frame.grid(padx=10, pady=10)


# start the app
root.mainloop()